from sklearn import datasets
import numpy as np
import matplotlib.pylab as plt
from sklearn.cluster import KMeans
from matplotlib.ticker import MaxNLocator

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, cluster_std=[1.0, 2.5, 0.5], random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


plt.rcParams['figure.figsize'] = [25, 20]

data = generate_data(500,1)

SSE_kmeans = []        
for k in range(2, 20):
    kmeans = KMeans(n_clusters=k)
    kmeans.fit(data)
    SSE_kmeans.append(kmeans.inertia_) 

ax = plt.figure().gca()
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.plot(range(2, 20), SSE_kmeans)
plt.title("Lakat metoda")
plt.xlabel("K")
plt.ylabel("SSE")
plt.tight_layout()
plt.show()

#na grafu se vidi da je lom funkcije kada je vrijednost clustera jednaka 3 
#možemo zaključiti da je optimalna količina clustera 3 za grupaciju ovih podataka
