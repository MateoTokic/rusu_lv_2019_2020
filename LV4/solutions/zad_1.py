import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
 
def non_func(x): #kreiranje funkcije y
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y
 
def add_noise(y): #kreiranje funkcije y sa šumom
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,100) #vektor x
y_true = non_func(x)  #vektor y kreiran iz funkcije non_func() kojoj predajemo vektor x
y_measured = add_noise(y_true) #simulacija y funkcije koja ima odstupanja
 
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno') 
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')   #prikaz "mjerene" i idealne funkcije y koja ovisi o x
plt.legend(loc = 4) 
 
np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] #indeksi koji se uzimaju za treniranje
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]  #indeksi koji se uzimaju za testiranje
 
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]
 
xtrain = x[indeksi_train]  #70% podataka koji se uzimaju za treniranje
ytrain = y_measured[indeksi_train] #70% podataka od vektora y koji se uzimaju za treniranje
 
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]
 
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
 
linearModel = lm.LinearRegression() #funkcija koja stvara linearni model
linearModel.fit(xtrain,ytrain)  #iz modela fitamo x i y da se napravi linearna funkcija za predviđanje
 
print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x') #ovim funkcijama se stvara theta0 odnosno theta1
 
ytest_p = linearModel.predict(xtest)   #y_test_p sadrži vrrijednosti koje funkcija linearModel.predict(x) vraća
MSE_test = mean_squared_error(ytest, ytest_p) #računanje  kvadratne udaljenosti/broj elemenata od napravljene funkcije iz modela 
 
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted') 
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

 
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)
plt.show()
