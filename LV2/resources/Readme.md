### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

LV2
Zadatak 1:
    Za provjeru ispravnih emailova koristio sam regularni izraz ([a-zA-Z.]+)@\S+. () izdvajaju samo prvi dio ispravnih emailova.
Zadatak 2:
    U ovom zadatku su za različite slučajeve korišteni različiti regularni izrazi.
Zadatak 3:
    Pomoću stack metode vrijednosti visina nadodane su vrijuednostima 0,1(koje označavaju mušku ili žensku osobu)a pomoću split metode vrijednosti su zasebno prikazane na grafu.
Zadatak 4:
    S funckijom random.choice generirano je 100 brojeva u rasponu 1-6 a s plt.hist vrijednosti su prikazane na histogramu.
Zadatak 5:
    Svaka linija datoteke rastavljena je na array string vrijednosti gdje su preko određenog indexa dobivene tražene vrijednosti koje su se dalje prikazale na grafu.
Zadatak 6:
    Množenjem rgb vrijednosti s koeficijentom 2 povećala se svjetlina.