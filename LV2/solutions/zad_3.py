import numpy as np
import matplotlib.pyplot as plt 

persons= np.random.choice([0,1],size=(10,))
print(persons)

heights=np.ones(10)

for i in range(0,10):
    if(persons[i]==1):
        heights[i]=np.random.normal(180,7)
    else:
        heights[i]=np.random.normal(167,7)

mf = np.stack((persons,heights), axis=1)

mf = mf[mf[:,0].argsort()]
mf2 = np.split(mf[:,1], np.unique(mf[:,0], return_index=True)[1][1:])
print(mf)
plt.figure(1)
plt.plot(mf2[0],'r')
plt.plot(mf2[1],'b')
plt.plot(mf2[0].mean(),'bo',color='red')
plt.plot(mf2[1].mean(),'bo',color='blue')
plt.show()

