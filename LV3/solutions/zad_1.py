import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars=pd.read_csv(r'C:\Users\mateo\Desktop\RUSU_LV\rusu_lv_2019_2020\LV3\resources\mtcars.csv')


#1.Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort) 
sort_mtcars = mtcars.sort_values(by=["mpg"])
print("Auti s najvećom potrošnjom:", sort_mtcars.head(5))
#2.Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
print("Auti s najmanjom potrošnjom:",sort_mtcars[sort_mtcars.cyl==8].tail(3))
#3.Kolika je srednja potrošnja automobila sa 6 cilindara? 
new_sort = mtcars.groupby('cyl')
print("Srednja potrošnja auta sa 6 cilindara: ", new_sort.mean().iloc[1,0])
#4.Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
sort_mtcars = mtcars[(mtcars.cyl == 4) & (mtcars['wt'].between(2.0,2.2))]
print("Srednja potršnja automobila s 4 cilindra mase između 2000 i 2200 lbs: ", sort_mtcars.groupby('cyl').mean().iloc[0,0])
#5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
print("Broj automobila s automatskim mjenjačem: ", mtcars[(mtcars.am == 1)].groupby('am').count().iloc[0,0])
print("Broj automobila s ručnim mjenjačem: ", mtcars[(mtcars.am == 0)].groupby('am').count().iloc[0,0])
#6.Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
print("Broj automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga: ", mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].groupby('am').count().iloc[0,0])
#7Kolika je masa svakog automobila u kilogramima? 
mtcars['kg'] = mtcars.wt * 0.4536 * 1000
print(mtcars.kg) 