import pandas as pd
import numpy as np
import matplotlib.pylab as plt

mtcars = pd.read_csv(r'C:\Users\student\Desktop\RUSU_LV4\rusu_lv_2019_2020\LV3\resources\mtcars.csv')

#1.Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
mtcars.sort_values(by=['cyl']).plot(x="cyl", y='mpg', kind="bar")
plt.show()
#2.Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
mtcars.sort_values(by='cyl').boxplot(by='cyl',column='mpg')
plt.show()
#3.Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
#potrošnju od automobila s automatskim mjenjačem?
mtcars_am = mtcars.groupby('am')['mpg'].mean().plot(kind='bar')
print(mtcars_am)
print('Automobili s automatskim mjenjacem imaju vecu potrosnju')
plt.show()
#4.Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#mjenjačem.
mtcars['qsec/hp'] = mtcars['qsec'] / mtcars['hp']
mtcars.groupby('am')['qsec/hp'].mean().plot(kind='bar')
plt.show()
