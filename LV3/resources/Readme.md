### mtcars.csv

Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models)
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset



### AirQualityRH.py

Skripta za dohvaćanje podataka o kvaliteti zraka pomocu REST API

Zadatak1: Pomoću Pandas biblioteke se učitavaju vrijednosti iz csv datoteke u pandas DataFrame objekt. Na vježbi smo dodavali nove stupce, izdvajali pojedine stupce ili određene redove iz DataFramea,...

Zadatak2: Korištenjem pandasa i matplotlib.plot vizualno smo prikazali podatke iz csv na različite načine.

Zadatak3: Korištenjem RESTfull servisa dohvatili smo podatke s interneta i dalje ih obradili pomoću pandasa i matplotliba.
