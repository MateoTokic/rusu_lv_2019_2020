import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import requests
import json
import urllib
import xml.etree.ElementTree as ET
import time

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'
 
airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)
 
print(airQualityHR)
 
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))
children=list(root)
i = 0
while True:
    
    try:
        obj = list(children[i])
    except:
         break
    
 
 
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    print(i,row_s)
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1
 
df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme')



# add date month and day designator
df['month'] =pd.to_datetime(df['vrijeme'],utc=True).dt.month
df['year'] = pd.to_datetime(df['vrijeme'],utc=True).dt.year
df['dayOfweek'] = pd.to_datetime(df['vrijeme'],utc=True).dt.dayofweek
print(df)

#2.Ispis tri datuma u godini kada je koncentracija PM10 bila najveća. 

print("Ispis tri datuma u godini kada je koncentracija PM10 bila najveća: ")
print(df.sort_values(by=["mjerenje"],ascending=False).head(3)['vrijeme'])


#3.Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca. 
df.groupby('month').mjerenje.count().plot(kind='bar')
plt.ylabel('Broj mjerenja')


#4.Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
df[(df['month'] == 7) | (df['month'] == 1)].groupby(by='month').boxplot(by='month',column='mjerenje')


#5.Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.
df.loc[df['dayOfweek'] >= 5, 'weekend'] = True
df.loc[df['dayOfweek'] < 5, 'weekend'] = False
print(df)

df[(df['weekend'] == True) | (df['weekend'] == False)].boxplot(by='weekend',column='mjerenje')
plt.show()
