fname = input('Enter file name: ')
try:
    fhand = open('../resources/'+fname)
    confidence = 0
    count = 0

    for line in fhand:
        line = line.rstrip()
        if(line.startswith('X-DSPAM-Confidence: ')):
            confidence += float(line[21:])
            count += 1
    average = confidence/float(count)
    print('Ime datoteke: ', fname)
    print('Average X-DSPAM-Confidence: ', average)
except:
    print('File not found!')
    exit()